<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app = new \Slim\App();

$app->get('/api/getuser', function(Request $request, Response $response) {
    echo 'Index';

    $db = new db();

    try {
        $db_connect = $db->connect();
        $select_user = $db_connect->query("SELECT * FROM makale")->fetchAll(PDO::FETCH_OBJ);


        return $response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withJson($select_user);


    } catch (PDOException $e) {
        return $response->withJson(
            array(
                "error" => array(
                    "text" => $e->getMessage(),
                    "code" => $e->getCode()
                )
            )
        );
    }

    $db = null;
});


$app->get('/api/isim={isim}&soyad={soyisim}', function(Request $request, Response $response, $args = []) {
    $isimObj = '';
    $isimObj->ad = $args['isim'];
    $isimObj->soyad = $args['soyisim'];

    return $response
        ->withStatus(200)
        ->withHeader('Content-Type','application/json')
        ->withJson($isimObj);
});

