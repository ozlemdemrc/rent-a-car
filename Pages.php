<?php


class HomeView extends \Slim\View
{
    public function render($template)
    {
        include 'pages/home.php';
    }
}


class ContactView extends \Slim\View
{
    public function render($template)
    {
        include 'pages/contact.php';
    }
}



class DealerView extends \Slim\View
{
    public function render($template)
    {
        include 'pages/dealer.php';
    }
}



class TestView extends \Slim\View
{
    public function render($template)
    {
        include 'pages/test.php';
    }
}
