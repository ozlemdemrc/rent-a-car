<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Cyprus Rent a Car</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome part -->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <!--css animation link-->
    <link rel="stylesheet" href="css-animation/animate.min.css">
    <!--hover css effect-->
    <link rel="stylesheet" href="css/hover.css">
    <!--slider links-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- JavaScript section starts-->
    <script type="text/javascript">

      //password validation for all forms
      function passwordLength1() {
        var password = document.getElementById("inputPassword").value;

        if(  password.length < 8){
          confirm( "Please enter min 8 character as password");
        }
      }

      function passwordLength2() {
        var password2 = document.getElementById("inputPasswordS").value;
        var password3 = document.getElementById("inputPasswordSC").value;

        if(  password2.length < 8 || password3.length < 8){
          confirm( "Please enter min 8 character as password");
        }
      }

      function passwordLength3() {
        var password4 = document.getElementById("inputPasswordD").value;
        var password5 = document.getElementById("inputPasswordDS").value;


        if(  password4.length < 8 || password5.length < 8){
          confirm( "Please enter min 8 character as password");
        }
      }

      //password validation ends

      // make colourful inputs for forms
      function makeColourful(takenValue) {
        takenValue.style.background = "#fbfbfb";
      }
    </script>
    <!-- JavaScript section ends-->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="col-lg-12 col-md-12" id="header">
    <!-- navigation bar -->
    <?php
        include_once 'parts/navbar.php';
    ?>

    <div class="modal fade" id="login">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="form-signin">
                        <h2 class="form-signin-heading">Log in</h2>
                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" id="inputEmail" class="form-control"  onfocus="makeColourful(this)" placeholder="Email address" required autofocus><br>
                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" id="inputPassword" class="form-control" onfocus="makeColourful(this)" placeholder="Password" required autofocus>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block"  onclick="passwordLength1()" type="submit">Sign in</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="signin">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="form-signin" action="registeruser.php" method="post">
                        <h2 class="form-signin-heading">Sign Up</h2>
                        <label for="inputEmail" class="sr-only">First Name</label>
                        <input type="text" id="fname" name="signinfname" class="form-control" placeholder="First Name" onfocus="makeColourful(this)" required autofocus><br>
                        <label for="inputEmail" class="sr-only">Last Name</label>
                        <input type="text" id="sname" name="signinsname" class="form-control" placeholder="Last Name" onfocus="makeColourful(this)" required autofocus><br>
                        <label for="inputEmail" class="sr-only">E-Mail</label>
                        <input type="email" id="semail" name="signinsemail" class="form-control" placeholder="Email address"  onfocus="makeColourful(this)"required autofocus><br>
                        <label for="inputEmail" class="sr-only">Password</label>
                        <input type="password" id="inputPasswordS"  name="signinPasswordS" class="form-control" placeholder="Password" onfocus="makeColourful(this)" required autofocus><br>
                        <label for="inputEmail" class="sr-only">Confirm Password</label>
                        <input type="password" id="inputPasswordSC" name="signininputPasswordSC" class="form-control" placeholder="Confirm Password" onfocus="makeColourful(this)" required autofocus><br>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" name="submit" onclick="passwordLength2()" type="submit">Sign in</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <?php
    include_once 'forms/dealer_form.html';

    ?>

    <!--end of navigation-->

</header>
<!-- header part ends-->
<!--main part ends -->
<main>
    <section class="searchcar contactus_container col-lg-12 col-md-12">
        <div class="container">
            <h2>Contact US</h2>
            <p style="margin-bottom:40px">Please let us know if you have an issue, questions or anything else.</p>
            <div class="col-lg-6 col-md-6 " >
                <i class="fa fa-mail-reply-all " aria-hidden="true"></i>
                <h4 style="font-size: 16px">Send email to us</h4>
                <h4 >contact@carboxcyprus.com</h4>
            </div>
            <div class="col-lg-6 col-md-6 ">
                <i class="fa fa-phone-square" aria-hidden="true"></i>
                <h4 style="font-size: 16px">Call us</h4>
                <h4>+9054121312323</h4>
            </div>
        </div>
    </section>
    <section class="col-lg-12 col-md-12 " style="background-color: #ffffff;">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Details</h2>
                <p class="text-center" >Find more information.</p>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">About us</a></li>
                    <li><a data-toggle="tab" href="#menu1">Information</a></li>
                </ul>

                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <h3>About us</h3>
                        <p>Sixt rent a car was founded in 1912 in Munich, Germany, and started out with a fleet of just three vehicles. As one of the first and most important international car rental companies in the world -  with over 100 years in the business - we have earned a trusted reputation as a global leading car rental provider. Always at the forefront of the industry, we were the first car rental company with a website and the first to accept mobile reservations. Today we are present in over 105 countries with branches in over 4,000 locations. You will be able to find our Sixt car rental services internationally, in almost every major city and tourist destination worldwide and at convenient locations such as airports, train stations, cruise ports, and hotels.</p>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <h3>More information</h3>
                        <p>Sixt is one of the biggest car rental companies in Europe with over 1,500 locations in an array of popular European travel destinations. If you need a rental car at a European airport or train station Sixt will be there with an affordable solution to your mobility needs. At all our European locations you will be able to choose from a wide range of premium vehicles and services. Whether you need a GPS, child seat, an additional driver or insurance coverage, we can help you upgrade your rent a car with the right add-ons to cover your requirements. Sixt is looking forward to giving you the means to move independently about Europe in a vehicle that is perfect for your trip. Check out some of the most popular car rental locations Sixt has in Europe below.
                            SIXT CAR RENTAL FLEET
                            Each Sixt rent a car location around the globe stands by our Drive First Class and Pay Economy motto offering premium car rental at attractive rates. We have over 222,000 rental vehicles in our fleet, which hosts models from some of the best car manufacturers in the world. Our rental cars are regularly replaced so you can feel confident that with Sixt you will be behind the wheel of a well-maintained and up-to-date vehicle. As the largest BMW, Audi & Mercedes-Benz car rental provider, we are also known for our affordable luxury car rental. Drive a prestige rent a car for less with Sixt luxury cars.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 footerthreebox">
                <h4>Contact</h4>
                <p><i class="fa fa-home" aria-hidden="true"></i>  Street</p>
                <p><i class="fa fa-envelope" aria-hidden="true"></i>  e175559@metu.edu.tr</p>
                <p><i class="fa fa-phone" aria-hidden="true"></i>  0 533 841 32 40</p>
                <p><i class="fa fa-globe" aria-hidden="true"></i>  www.metu.edu.tr</p>
            </div>
            <div class="col-lg-4 col-md-4 footerthreebox">
                <h4>About us</h4>
                <ul class="list-unstyled">
                    <li>FAQ</li>
                    <li>Contact us</li>
                    <li>Europcar on mobile</li>
                    <li>Find a rental location</li>
                    <li>Green policy</li>
                    <li>Privacy Policy</li>
                    <li>Booking terms</li>
                    <li>Site map</li>
                    <li>Mobile App</li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 footerthreebox">
                <h4>Social media</h4>
                <i class="socialmedia fa fa-facebook" aria-hidden="true"></i>
                <i class="socialmedia fa fa-twitter" aria-hidden="true"></i>
                <i class="socialmedia fa fa-linkedin" aria-hidden="true"></i>
                <i class="socialmedia fa fa-youtube" aria-hidden="true"></i>
                <br>
                <form  method="post" action="subscribe.php">
                    <input type="text" placeholder="Subscribe" name="email">
                    <button class="btn btn-primary" name="submit" >Subscribe</button>
                </form>
            </div>
            <div class="col-lg-12 col-md-12">
                <p>&copy; Copyright Cyprus rent a car</p>
            </div>
        </div>
    </div>
</footer>
<!-- wow js file-->
<script src="js/wow.min.js"></script>
<script>
  new WOW().init();
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>