<nav id="navbar-fixed-top" class="navbar navbar-default navbar-fixed-top" >
    <div class="container">
        <div class="navbar-header">
            <button type="button"class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false" aria-controls="navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div><a  href="home"> <img src="images/logo.jpg" class="logo"/></a></div>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="home" class="hvr-underline-from-center">Home</a></li>
                <!--                    <li><a href="vehicles.php" class="hvr-underline-from-center">Vehicles</a></li>-->
                <!--                    <li><a class="dropdown-toggle hvr-underline-from-center" data-toggle="dropdown" href="#" >Details-->
                <!--                            <span class="caret"></span></a>-->
                <!--                        <ul class="dropdown-menu">-->
                <!--                            <li><a href="addcardealer.php" class="hvr-underline-from-center">Add car</a></li>-->
                <!--                            <li><a href="dealers.php" class="hvr-underline-from-center">Dealers</a></li>-->
                <!--                            <li><a href="locations.php" class="hvr-underline-from-center">Locations</a></li>-->
                <!--                        </ul></li>-->
                <li><a href="#" data-toggle="modal" data-target="#dealersignin" class="hvr-underline-from-center" style="color: darkred;"><span class="glyphicon glyphicon-check" style="color: darkred;"></span> Dealer Register</a></li>
                <li><a href="#" data-toggle="modal" data-target="#signin" class="hvr-underline-from-center"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="#" data-toggle="modal" data-target="#login" class="hvr-underline-from-center" ><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <li><a href="contact" class="hvr-underline-from-center">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
