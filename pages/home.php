<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test Deployment Cyprus Rent a Car</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->


    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157263390-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-157263390-1');
    </script>

    <meta name="title" content="Rent a Car in Cyprus Anytime Anywhere Carbox Cyprus">
    <meta name="description" content="Cyprus Car Box is rent a car platform. Dealers can create an account and publish their cars for rental.">
    <meta name="keywords" content="rent a car, rent car, cyprus rent car, cyprus rent a car, car rental in cyprus, cyprus, car, rent, dealer, holiday in cyprus">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-MWMMBLH');</script>
    <!-- End Google Tag Manager -->
    <script data-ad-client="ca-pub-2469701580470390" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- font awesome part -->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <!--css animation link-->
    <link rel="stylesheet" href="css-animation/animate.min.css">
    <!--hover css effect-->
    <link rel="stylesheet" href="css/hover.css">
    <!--slider links-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- JavaScript section starts-->
    <script type="text/javascript">

      //password validation for all forms
      function passwordLength1() {
        var password = document.getElementById("inputPassword").value;

        if(  password.length < 8){
          confirm( "Please enter min 8 character as password");
        }
      }

      function passwordLength2() {
        var password2 = document.getElementById("inputPasswordS").value;
        var password3 = document.getElementById("inputPasswordSC").value;

        if(  password2.length < 8 || password3.length < 8){
          confirm( "Please enter min 8 character as password");
        }
      }

      function passwordLength3() {
        var password4 = document.getElementById("inputPasswordD").value;
        var password5 = document.getElementById("inputPasswordDS").value;


        if(  password4.length < 8 || password5.length < 8){
          confirm( "Please enter min 8 character as password");
        }
      }

      //password validation ends

      // make colourful inputs for forms
      function makeColourful(takenValue) {
        takenValue.style.background = "#fbfbfb";
      }
    </script>
    <!-- JavaScript section ends-->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MWMMBLH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header class="col-lg-12 col-md-12" id="header">
    <!-- navigation bar -->
    <?php
        include_once 'parts/navbar.php';
    ?>
    <div class="modal fade" id="login">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="form-signin">
                        <h2 class="form-signin-heading">Test Log in</h2>
                        <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" id="inputEmail" class="form-control"  onfocus="makeColourful(this)" placeholder="Email address" required autofocus><br>
                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" id="inputPassword" class="form-control" onfocus="makeColourful(this)" placeholder="Password" required autofocus>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block"  onclick="passwordLength1()" type="submit">Sign in</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="signin">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="form-signin" action="registeruser.php" method="post">
                        <h2 class="form-signin-heading">Sign Up</h2>
                        <label for="inputEmail" class="sr-only">First Name</label>
                        <input type="text" id="fname" name="signinfname" class="form-control" placeholder="First Name" onfocus="makeColourful(this)" required autofocus><br>
                        <label for="inputEmail" class="sr-only">Last Name</label>
                        <input type="text" id="sname" name="signinsname" class="form-control" placeholder="Last Name" onfocus="makeColourful(this)" required autofocus><br>
                        <label for="inputEmail" class="sr-only">E-Mail</label>
                        <input type="email" id="semail" name="signinsemail" class="form-control" placeholder="Email address"  onfocus="makeColourful(this)"required autofocus><br>
                        <label for="inputEmail" class="sr-only">Password</label>
                        <input type="password" id="inputPasswordS"  name="signinPasswordS" class="form-control" placeholder="Password" onfocus="makeColourful(this)" required autofocus><br>
                        <label for="inputEmail" class="sr-only">Confirm Password</label>
                        <input type="password" id="inputPasswordSC" name="signininputPasswordSC" class="form-control" placeholder="Confirm Password" onfocus="makeColourful(this)" required autofocus><br>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" name="submit" onclick="passwordLength2()" type="submit">Sign in</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <?php
        include_once 'forms/dealer_form.html';

    ?>
    
    <!--end of navigation-->

</header>
<!-- header part ends-->
<!--main part ends -->
<main>
<!--    <section>-->
<!--        <div class="slider col-lg-12 col-md-12" >-->
<!--            <div id="myCarousel" class="carousel slide" data-ride="carousel">-->
<!--                <!-- Indicators -->-->
<!--                <ol class="carousel-indicators" >-->
<!--                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
<!--                    <li data-target="#myCarousel" data-slide-to="1"></li>-->
<!--                    <li data-target="#myCarousel" data-slide-to="2"></li>-->
<!--                    <li data-target="#myCarousel" data-slide-to="3"></li>-->
<!--                    <li data-target="#myCarousel" data-slide-to="4"></li>-->
<!--                </ol>-->
<!--                <!-- Wrapper for slides -->-->
<!--                <div class="carousel-inner" role="listbox">-->
<!--                    <div class="item active">-->
<!--                        <img src="images/slider/1.png" alt="Los Angeles" style="width:100%; max-height: 400px;">-->
<!--                        <div class="container">-->
<!--                            <div class="carousel-caption d-none d-md-block text-right">-->
<!--                                <h1>Save up to 25%</h1>-->
<!--                                <p>Daily, weekly, monthly, short and long term car rental</p>-->
<!--                                <p><a class="btn btn-lg btn-primary" href="#" data-toggle="modal" data-target="#signin" role="button" >Sign Up</a></p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="item">-->
<!--                        <img src="images/slider/2.png" alt="Chicago" style="width:100%;max-height: 375px;">-->
<!--                        <div class="container">-->
<!--                            <div class="carousel-caption d-none d-md-block text-left">-->
<!--                                <h1>No hidden charges</h1>-->
<!--                                <p>Daily, weekly, monthly, short and long term car rental</p>-->
<!--                                <p><a class="btn btn-lg btn-danger" href="#" data-toggle="modal" data-target="#signin" role="button" >Sign Up</a></p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <div class="item">-->
<!--                        <img src="images/slider/3.png" alt="New york" style="width:100%;max-height: 375px;">-->
<!--                        <div class="container">-->
<!--                            <div class="carousel-caption d-none d-md-block text-left">-->
<!--                                <h1>Only new rental cars</h1>-->
<!--                                <p>Daily, weekly, monthly, short and long term car rental</p>-->
<!--                                <p><a class="btn btn-lg btn-success" href="#" data-toggle="modal" data-target="#signin" role="button" >Sign Up</a></p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <img src="images/slider/4.png" alt="New york" style="width:100%;max-height: 375px;">-->
<!--                        <div class="container">-->
<!--                            <div class="carousel-caption d-none d-md-block text-left">-->
<!--                                <h1>24 hour roadside assistance</h1>-->
<!--                                <p>Daily, weekly, monthly, short and long term car rental</p>-->
<!--                                <p><a class="btn btn-lg btn-default" href="#" data-toggle="modal" data-target="#signin" role="button" >Sign Up</a></p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="item">-->
<!--                        <img src="images/slider/5.png" alt="New york" style="width:100%;max-height: 375px;">-->
<!--                        <div class="container">-->
<!--                            <div class="carousel-caption d-none d-md-block text-left">-->
<!--                                <h1>24/7 service in most major airports</h1>-->
<!--                                <p>Daily, weekly, monthly, short and long term car rental</p>-->
<!--                                <p><a class="btn btn-lg btn-warning" href="#" data-toggle="modal" data-target="#signin" role="button" >Sign Up</a></p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!---->
<!--                    <!-- Left and right controls -->-->
<!--                    <a class="left carousel-control" href="#myCarousel" data-slide="prev" >-->
<!--                        <span class="glyphicon glyphicon-chevron-left"></span>-->
<!--                        <span class="sr-only">Previous</span>-->
<!--                    </a>-->
<!--                    <a class="right carousel-control" href="#myCarousel" data-slide="next" >-->
<!--                        <span class="glyphicon glyphicon-chevron-right"></span>-->
<!--                        <span class="sr-only">Next</span>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--    </section>-->
    <section>
        <div class="col-lg-12 col-md-12 booknow" style="padding:100px 0 100px 0">
            <h2>Book Now</h2>
            <h4>Select the most suitable car</h4>
            <form class="form-inline formmedia" style="background: rgba(255,255,255,.7); padding: 80px; width: 90%; margin: 0 auto" action="searchresult.php" method="post">
                <div class="pickui form-group">
                    <label >Pickup:</label>
                    <select class="select form-control" id="select1" name="selectfirst">
                        <option value="First Choice">
                            Lefkoşa
                        </option>
                        <option value="Second Choice">
                            Güzelyurt
                        </option>
                        <option value="Third Choice">
                            Girne
                        </option>
                        <option value="Fourth Choice">
                            Magusa
                        </option>
                    </select>
                </div>
                <div class="pickui form-group">
                    <label >Return:</label>
                    <select class="select form-control" id="select2" name="selectsecond">
                        <option value="First Choice">
                            Lefkoşa
                        </option>
                        <option value="Second Choice">
                            Güzelyurt
                        </option>
                        <option value="Third Choice">
                            Girne
                        </option>
                        <option value="Fourth Choice">
                            Magusa
                        </option>
                    </select>
                </div>
                <div class="pickui form-group">
                    <label for="datetime1">Pickup Time:</label>
                    <input type="date" class="form-control" id="datetime1" name="selectthird">
                    <select class="select form-control" name="selectfourth" id="time1">
                        <option value="1:00 AM">1:00 AM</option>
                        <option value="2:00 AM">2:00 AM</option>
                        <option value="3:00 AM">3:00 AM</option>
                        <option value="4:00 AM">4:00 AM</option>
                        <option value="5:00 AM">5:00 AM</option>
                        <option value="6:00 AM">6:00 AM</option>
                        <option value="7:00 AM">7:00 AM</option>
                        <option value="8:00 AM">8:00 AM</option>
                        <option value="9:00 AM">9:00 AM</option>
                        <option value="10:00 AM">10:00 AM</option>
                        <option value="11:00 AM">11:00 AM</option>
                        <option value="12:00 PM">12:00 PM</option>
                        <option value="1:00 PM">1:00 PM</option>
                        <option value="2:00 PM">2:00 PM</option>
                        <option value="3:00 PM">3:00 PM</option>
                        <option value="4:00 PM">4:00 PM</option>
                        <option value="5:00 PM">5:00 PM</option>
                        <option value="6:00 PM">6:00 PM</option>
                        <option value="7:00 PM">7:00 PM</option>
                        <option value="8:00 PM">8:00 PM</option>
                        <option value="9:00 PM">9:00 PM</option>
                        <option value="10:00 PM">10:00 PM</option>
                        <option value="11:00 PM">11:00 PM</option>
                        <option value="0:00 AM">0:00 PM</option>
                    </select>
                </div>
                <div class="pickui form-group">
                    <label for="datetime2">Return Time:</label>
                    <input type="date" class="form-control" id="datetime2" name="selectfifth">
                    <select class="select form-control" name="selectsixth" id="time2">
                        <option value="1:00 AM">1:00 AM</option>
                        <option value="2:00 AM">2:00 AM</option>
                        <option value="3:00 AM">3:00 AM</option>
                        <option value="4:00 AM">4:00 AM</option>
                        <option value="5:00 AM">5:00 AM</option>
                        <option value="6:00 AM">6:00 AM</option>
                        <option value="7:00 AM">7:00 AM</option>
                        <option value="8:00 AM">8:00 AM</option>
                        <option value="9:00 AM">9:00 AM</option>
                        <option value="10:00 AM">10:00 AM</option>
                        <option value="11:00 AM">11:00 AM</option>
                        <option value="12:00 PM">12:00 PM</option>
                        <option value="1:00 PM">1:00 PM</option>
                        <option value="2:00 PM">2:00 PM</option>
                        <option value="3:00 PM">3:00 PM</option>
                        <option value="4:00 PM">4:00 PM</option>
                        <option value="5:00 PM">5:00 PM</option>
                        <option value="6:00 PM">6:00 PM</option>
                        <option value="7:00 PM">7:00 PM</option>
                        <option value="8:00 PM">8:00 PM</option>
                        <option value="9:00 PM">9:00 PM</option>
                        <option value="10:00 PM">10:00 PM</option>
                        <option value="11:00 PM">11:00 PM</option>
                        <option value="0:00 AM">0:00 PM</option>
                    </select>
                </div>
                <br>
                <br>
                <button type="submit" class="searchbtn "><span class="glyphicon glyphicon-search" aria-hidden="true"></span><a href="searchresult.php"> Search a Car</a></button>
            </form>
        </div>
    </section>
    <section class="searchcar col-lg-12 col-md-12">
        <div class="container">
            <h2>How to Rent a Car</h2>
            <p>Rent a car online has never been this easy</p>
            <div class="col-lg-4 col-md-4 " >
                <i class="fa fa-map " aria-hidden="true"></i>
                <h4>Find a location or dealer</h4>
            </div>
            <div class="col-lg-4 col-md-4 ">
                <i class="fa fa-car" aria-hidden="true"></i>
                <h4>Find a Vehicle</h4>
            </div>
            <div class="col-lg-4 col-md-4 ">
                <i class="fa fa-database" aria-hidden="true"></i>
                <h4>Rent a car</h4>
            </div>
            <div class="input-group" style="padding: 50px 60px;" >
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
        <button class="btn btn-warning" type="button">Search on website</button>
      </span>
            </div>
        </div>
    </section>
<!--    <section class="col-lg-12 col-md-12 cheapest-car">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <h2>Dealer List</h2>-->
<!--                <p>Most popular dealers</p>-->
<!--                <div class="col-md-4 ">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-danger"><span class="label label-danger pull-right">- 9%</span> Dealer1 </h4>-->
<!--                        <p>Information on dealer Information on dealer</p>-->
<!--                        <a href="#" class="btn btn-primary " role="button">See details</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4 ">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-success"><span class="label label-success pull-right">+ 3%</span> Dealer2 </h4>-->
<!--                        <p>Information on dealer Information on dealer</p>-->
<!--                        <a href="#" class="btn btn-primary" role="button">See details</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-primary"><span class="label label-primary pull-right">201</span> Dealer3 </h4>-->
<!--                        <p>Information on dealer Information on dealer</p>-->
<!--                        <a href="#" class="btn btn-primary" role="button">See details</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-primary"><span class="label label-primary pull-right">201</span> Dealer4 </h4>-->
<!--                        <p>Information on dealer Information on dealer</p>-->
<!--                        <a href="#" class="btn btn-primary" role="button">See details</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-primary"><span class="label label-primary pull-right">201</span> Dealer5 </h4>-->
<!--                        <p>Information on dealer Information on dealer</p>-->
<!--                        <a href="#" class="btn btn-primary" role="button">See details</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-primary"><span class="label label-primary pull-right">201</span> Dealer6 </h4>-->
<!--                        <p>Information on dealer Information on dealer</p>-->
<!--                        <a href="#" class="btn btn-primary" role="button">See details</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section id="dealer" class="dealer col-lg-12 col-md-12">-->
<!--        <div class="container">-->
<!--            <h2>Vehicles</h2>-->
<!--            <div class="row">-->
<!--                <div class="col-sm-3 col-md-3 ">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car1</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm-3 col-md-3 ">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car2</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm-3 col-md-3">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car3</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm-3 col-md-3">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car4</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm-3 col-md-3">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car5</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm-3 col-md-3">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car6</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm-3 col-md-3">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car7</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-sm-3 col-md-3">-->
<!--                    <div class="thumbnail hvr-glow">-->
<!--                        <img src="images/242-200.png" alt="car1">-->
<!--                        <div class="caption">-->
<!--                            <h3>Car8</h3>-->
<!--                            <p>...</p>-->
<!--                            <p><a href="#" class="btn btn-primary" role="button">Book now</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section class="col-lg-12 col-md-12 boxes">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <h2>Companies</h2>-->
<!--                <p>Supporter companies</p>-->
<!--                <div class="col-md-4 ">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-danger"><span class="label label-danger pull-right">- 9%</span> Company1</h4>-->
<!--                        <p>Information on company Information on company Information on company</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4 ">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-success"><span class="label label-success pull-right">+ 3%</span> Company2 </h4>-->
<!--                        <p>Information on company Information on company Information on company</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-4">-->
<!--                    <div class="well  hvr-float-shadow">-->
<!--                        <i class="fa fa-map" aria-hidden="true"></i>-->
<!--                        <h4 class="text-primary"><span class="label label-primary pull-right">201</span> Company3 </h4>-->
<!--                        <p>Information on company Information on companyInformation on company</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section class="col-lg-12 col-md-12 " style="background-color: #ffffff;">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <h2 class="text-center">Details</h2>-->
<!--                <p class="text-center" >Find more information.</p>-->
<!--                <ul class="nav nav-tabs">-->
<!--                    <li class="active"><a data-toggle="tab" href="#home">About us</a></li>-->
<!--                    <li><a data-toggle="tab" href="#menu1">Information</a></li>-->
<!--                </ul>-->
<!---->
<!--                <div class="tab-content">-->
<!--                    <div id="home" class="tab-pane fade in active">-->
<!--                        <h3>About us</h3>-->
<!--                        <p>Sixt rent a car was founded in 1912 in Munich, Germany, and started out with a fleet of just three vehicles. As one of the first and most important international car rental companies in the world -  with over 100 years in the business - we have earned a trusted reputation as a global leading car rental provider. Always at the forefront of the industry, we were the first car rental company with a website and the first to accept mobile reservations. Today we are present in over 105 countries with branches in over 4,000 locations. You will be able to find our Sixt car rental services internationally, in almost every major city and tourist destination worldwide and at convenient locations such as airports, train stations, cruise ports, and hotels.</p>-->
<!--                    </div>-->
<!--                    <div id="menu1" class="tab-pane fade">-->
<!--                        <h3>More information</h3>-->
<!--                        <p>Sixt is one of the biggest car rental companies in Europe with over 1,500 locations in an array of popular European travel destinations. If you need a rental car at a European airport or train station Sixt will be there with an affordable solution to your mobility needs. At all our European locations you will be able to choose from a wide range of premium vehicles and services. Whether you need a GPS, child seat, an additional driver or insurance coverage, we can help you upgrade your rent a car with the right add-ons to cover your requirements. Sixt is looking forward to giving you the means to move independently about Europe in a vehicle that is perfect for your trip. Check out some of the most popular car rental locations Sixt has in Europe below.-->
<!--                            SIXT CAR RENTAL FLEET-->
<!--                            Each Sixt rent a car location around the globe stands by our Drive First Class and Pay Economy motto offering premium car rental at attractive rates. We have over 222,000 rental vehicles in our fleet, which hosts models from some of the best car manufacturers in the world. Our rental cars are regularly replaced so you can feel confident that with Sixt you will be behind the wheel of a well-maintained and up-to-date vehicle. As the largest BMW, Audi & Mercedes-Benz car rental provider, we are also known for our affordable luxury car rental. Drive a prestige rent a car for less with Sixt luxury cars.</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
</main>
<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 footerthreebox">
                <h4>Contact</h4>
                <p><i class="fa fa-home" aria-hidden="true"></i>  Street</p>
                <p><i class="fa fa-envelope" aria-hidden="true"></i>  e175559@metu.edu.tr</p>
                <p><i class="fa fa-phone" aria-hidden="true"></i>  0 533 841 32 40</p>
                <p><i class="fa fa-globe" aria-hidden="true"></i>  www.metu.edu.tr</p>
            </div>
            <div class="col-lg-4 col-md-4 footerthreebox">
                <h4>About us</h4>
                <ul class="list-unstyled">
                    <li>FAQ</li>
                    <li>Contact us</li>
                    <li>Europcar on mobile</li>
                    <li>Find a rental location</li>
                    <li>Green policy</li>
                    <li>Privacy Policy</li>
                    <li>Booking terms</li>
                    <li>Site map</li>
                    <li>Mobile App</li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 footerthreebox">
                <h4>Social media</h4>
                <i class="socialmedia fa fa-facebook" aria-hidden="true"></i>
                <i class="socialmedia fa fa-twitter" aria-hidden="true"></i>
                <i class="socialmedia fa fa-linkedin" aria-hidden="true"></i>
                <i class="socialmedia fa fa-youtube" aria-hidden="true"></i>
                <br>
                <form  method="post" action="subscribe.php">
                    <input type="text" placeholder="Subscribe" name="email">
                    <button class="btn btn-primary" name="submit" >Subscribe</button>
                </form>
            </div>
            <div class="col-lg-12 col-md-12">
                <p>&copy; Copyright Cyprus rent a car</p>
            </div>
        </div>
    </div>
</footer>
<!-- wow js file-->
<script src="js/wow.min.js"></script>
<script>
  new WOW().init();
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

</body>
</html>