<?php
session_start();

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

require 'vendor/autoload.php';
//require 'src/config/db.php';
require 'Pages.php';

$app = new \Slim\Slim();


$app = new \Slim\Slim(array(
    'view'
));

$app->get('/', function () use ($app) {
    $app->view = new HomeView();
    $app->render('home.php');
});

$app->get('/home', function () use ($app) {
    $app->view = new HomeView();
    $app->render('home.php');
});


$app->get('/contact', function () use ($app) {
    $app->view = new ContactView();
    $app->render('contact.php');
});


$app->get('/dealer', function () use ($app) {
    $app->view = new DealerView();
    $app->render('home.php');
});



$app->get('/hello/:name', function ($name) {
    echo "Hello, " . $name;
});




// Routes

//require '../src/api/endpoints.php';
//require '../src/routes/routes.php';



$app->run();


